import english from './locales/en';
import german from './locales/de';

export default {
  de: german,
  en: english,
};
